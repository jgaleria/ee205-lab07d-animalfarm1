############################
#
# Author: Joshua Galeria
# Date  : 2/19/2022
# Name  : Makefile
# Desc  : Makefile for program
#
###########################

## Prereq
CC = gcc
CFLAGS = -g -Wall -Wextra
TARGET = main
all: $(TARGET)
 
## Create .o from dependencies
addCats.o: addCats.c addCats.h 
	$(CC) $(CFLAGS) -c addCats.c

updateCats.o: updateCats.c updateCats.h
	$(CC) $(CFLAGS) -c updateCats.c
 
reportCats.o: reportCats.c reportCats.h
	$(CC) $(CFLAGS) -c reportCats.c

deleteCats.o: deleteCats.c deleteCats.h
	$(CC) $(CFLAGS) -c deleteCats.c

catDatabase.o: catDatabase.c catDatabase.h
	$(CC) $(CFLAGS) -c catDatabase.c

errorMessage.o: errorMessage.c errorMessage.h
	$(CC) $(CFLAGS) -c errorMessage.c

## Main
main.o: main.c 
	$(CC) $(CFLAGS) -c main.c

## Test target

main: main.o addCats.o updateCats.o reportCats.o deleteCats.o catDatabase.o 
	$(CC) $(CFLAGS) -o $(TARGET) main.o addCats.o updateCats.o reportCats.o deleteCats.o catDatabase.o
  
clean:
	rm -f $(TARGET) *.o

test: main
	./main
