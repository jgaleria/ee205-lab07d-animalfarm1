////////////////////////////////////
//
// Author : Joshua Galeria
// Date   : 3/6/2022
// Desc   : Header file for Update
//
///////////////////////////////////

#pragma once
#include "catDatabase.h"

bool updateCatName( int index, char newName[] );

bool fixCat( int index );

bool updateCatWeight( int index, float newWeight );

bool updateCollar1 ( int index, enum Color newColor );

bool updateCollar2 ( int index, enum Color newColor );

bool updateLicense ( int index, unsigned long long newLicense );
