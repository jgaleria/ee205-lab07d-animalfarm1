//////////////////////////////
//
// Author : Joshua Galeria
// Date   : 3/6/2022
// Desc   : Header for add cats
//
/////////////////////////////

#pragma once 
#include "catDatabase.h"

bool addCat ( const char name[], const enum Gender gender, const enum Breed breed, bool isFixed, float weight, enum Color collar1, enum Color collar2, unsigned long long license );
