//////////////////////////
//
// Author : Joshua Galeria
// Date   : 3/6/2022
// Desc   : Header file for delete
//
/////////////////////////

#pragma once
#include "catDatabase.h"

void deleteAllCats();

bool deleteCat( int index );
