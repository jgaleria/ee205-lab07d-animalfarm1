///////////////////////////////////
//
// Author : Joshua Galeria
// Date   : 3/6/2022
// Desc   : Header file for report
//
//////////////////////////////////

#pragma once
#include "catDatabase.h"

bool printCat( int index );

void printAllCats();

int findCat( char name[] );
