//////////////////////////
//
// Author : Joshua Galeria
// Date   : 2/18/2022
// Name   : updateCats.c
// Desc   : Update for cats
//
/////////////////////////

//Header
#include "catDatabase.h"
#include "updateCats.h"
#include <stdio.h>
#include <string.h>

bool updateCatName( int index, char newName[] ) {
   
   //Error check from catDatabase.c
   if ( nameIsEmpty(newName) ) {
      return false;
   } 
   
   if ( isNameUnique(newName) ) { 
      return false;
   } 
   
   //Update the cat name
   strcpy(Database[index].name, newName);
   return true;
} 

bool fixCat( int index ) {
   Database[index].isFixed = true;
   return true;
} 

bool updateCatWeight( int index, float newWeight ) {
   
   //Error check from catDatabase.c
   if ( weightValid(newWeight) ) {
      return false;
   } 
   
   //Update cat weight
   Database[index].weight = newWeight;
   return true;
} 


bool updateCollar1 ( int index, enum Color newColor ) {
   Database[index].collar1 = newColor;
   return true;
} 

bool updateCollar2 ( int index, enum Color newColor ) { 
   Database[index].collar2 = newColor;
   return true;
}

bool updateLicense ( int index, unsigned long long newLicense ) {
   Database[index].license = newLicense;
   return true;
}
